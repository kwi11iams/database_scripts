"""Compare audit and worklistExome files.

Author: Katie Williams
Created: 06/04/2022
"""

import csv

output = "../outputs/worklists_to_upload.txt"


def checkIfDuplicates(x):
    """Check if a given list contains any duplicates."""
    if len(x) == len(set(x)):
        return False
    else:
        return True


def detectDuplicates(x):
    """Find duplicate entries in a given list."""
    list1 = []
    for entry in x:
        if entry not in list1:
            list1.append(entry)
        else:
            print(entry, " is a duplicate.")


# Set locations of worklist files that are being compared
audit_file = "audit_twist_worklists.txt"
worklistExome_file = "../outputs/worklistExome_filenames.txt"

# Load text files as lists
with open(audit_file) as w:
    # This way ensures any empty lines are ignored
    audit_lines = [line.strip() for line in w if line.strip()]

with open(worklistExome_file) as w:
    # This way ensures any empty lines are ignored
    worklistExome_lines = [line.strip() for line in w if line.strip()]

# Remove empty lines
audit_lines = list(filter(None, audit_lines))
worklistExome_lines = list(filter(None, worklistExome_lines))

# Check lengths
if len(audit_lines) == len(worklistExome_lines):
    print("\nBoth files are the same length, ", len(audit_lines),
          " lines each.")

else:
    print("Files different lengths. Audit = ", len(audit_lines),
          ", worklistExome = ", len(worklistExome_lines))

# Check for duplicates
if checkIfDuplicates(audit_lines):
    print("\nAudit has duplicates:")
    detectDuplicates(audit_lines)
else:
    print("\nAudit has NO duplicates.")


if checkIfDuplicates(worklistExome_lines):
    print("worklistExome has duplicates.")
    detectDuplicates(worklistExome_lines)
else:
    print("worklistExome has NO duplicates.")

# Compare lists
audit_lines.sort()
worklistExome_lines.sort()
if audit_lines == worklistExome_lines:
    print("\nFiles are identical")
else:
    print("\n***Investigation needed, files are not the same***")

print("Worklists in audit, not in worklistExome:")
for a in audit_lines:
    if a not in worklistExome_lines:
        print(a)
print("Worklists in worklistExome, not in audit:")
for w in worklistExome_lines:
    if w not in audit_lines:
        print(w)

# print(worklistExome_lines[-5:])
# print(audit_lines[-5:])

# Output Worklists in worklistExome, not in audit
# ie worklists to upload
with open(output, 'w') as o:

    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    for w in worklistExome_lines:
        if w not in audit_lines:
            write.writerow([w])
