"""Script 3, remove artefacts and refine variant list file.

Author: Katie Williams
Date created: 14/04/22

Input: variant_list.txt
Output: refined_list.txt, artefact_list.txt
"""

import pandas as pd
import re

# Set variant thresholds
artefact_score = 300  # Caller(Score)
artefact_freq = 20  # Caller(%Variant)
maybe_artefact_freq = 25  # Caller(%Variant)
coverage_limit = 16  # Caller(Cov;CovF;CovR)

# File locations
input_file = "../outputs/variant_list.txt"
output_file = "../outputs/refined_list.txt"
artefact_output = "../outputs/artefact_list.txt"

# Create empty output and artefact files
open(output_file, 'w').close()
open(artefact_output, 'w').close()

# Load input file as dataframe
df = pd.read_csv(input_file, sep="\t")
# print("Original dataframe is length", len(df))

# Sort dataframe so worklist is an ascending list (low to high)

# Remove variants from reanalysis
# i.e. a duplicate variant with the same StarLIMS_ID
# Only keep the first time a variant detected in a patient
bool_series = df.duplicated(subset=["StarLIMS_ID", "GenomeChange"],
                            keep='first')
df = df[~bool_series]

# Create artefact columns, with default value False
df['pred_artefact'] = False
df['poss_artefact'] = False

# Loop through remaining variants, and flag artefacts
for i in range(0, len(df)):

    # Obtain variant scores and frequencies from within brackets of the string
    # P(score)|HC(score), P(freq)|HC(freq)
    var_score = re.findall(r"\((\d+)\)", df["Caller(Score)"][i])
    var_freq = re.findall(r"\((\d+)\)", df["Caller(%Variant)"][i])

    # Strings to integers
    var_score = [int(i) for i in var_score]
    var_freq = [int(i) for i in var_freq]

    # Coverage...?
    var_cov = re.findall(r"(?<=\()([^)]+)(?=\))", df["Caller(Cov;CovF;CovR)"][i])
    read_count = [int(i.split(";")[0]) for i in var_cov]

    # Homozygote
    hetero_homo = df.loc[i, 'Variant'].split(" ")[0]

    # If variant below threshold score or frequency
    if any(score < artefact_score for score in var_score) | all(freq < artefact_freq for freq in var_freq):

        # Flag as an artefact so it can be added to artefact list later
        df.loc[i, 'pred_artefact'] = True

    # If frequency is below (higher) threshold frequency
    # elif HC_var_freq < maybe_artefact_freq:
    elif all(freq < maybe_artefact_freq for freq in var_freq):

        # Flag as possible artefact
        df.loc[i, 'poss_artefact'] = True

    # If variant not a homozygote and has a variant frequency of >90
    elif hetero_homo == "Heterozygous" and any(freq < maybe_artefact_freq for freq in var_freq):
        # Flag as possible artefact
        df.loc[i, 'poss_artefact'] = True

    # If variant has a read count lower than threshold
    elif all(count < coverage_limit for count in read_count):
        # Flag as possible artefact
        df.loc[i, 'poss_artefact'] = True


# Create artefact list (txt file)
artefact_df = df[df['pred_artefact'] == True]
artefact_df.to_csv(artefact_output, sep='\t', index=False)

# Create output list (txt file)
output_df = df[df['pred_artefact'] == False]
output_df.to_csv(output_file, sep='\t', index=False)
