# Script 4
# Copy columns to relevant output files
#!/bin/bash

# File locations
input_file="../outputs/refined_list.txt"
#occurence_output="../outputs/occurence_model.txt"
temp_file_output="../outputs/temp.txt"
variant_output="../outputs/variant_model.txt"

# Obtain variant columns
# awk -F '\t' -v OFS='\t' '{print $5,$6,$7,$8,$12,$13,$14,$15,$16,$17,$19,$20,$21,$22,$23}' $input_file > $temp_file_output
awk -F '\t' -v OFS='\t' '{print $1,$5,$6,$7,$8,$12,$13,$14,$15,$16,$17,$19,$20,$21,$22,$23}' $input_file > $temp_file_output
# Sort and Remove duplicates
# Sort first by Chr, the referenceposition, then genomehchange
# tail -n +2 $temp_file_output | sort -t$'\t' -k6V,6 -k1n,1 -k8g,8 | cat <(head -1 $temp_file_output) - | uniq > ../outputs/temp2.txt
tail -n +2 $temp_file_output | sort -t$'\t' -k7V,7 -k2n,2 -k9g,9 -k1nr,1 | cat <(head -1 $temp_file_output) - | uniq > ../outputs/temp2.txt

# Problem of duplicates by VariantType
# Only keep the most recent varaint, so the VariantType is the most recent allocation
awk -F '\t' -v OFS='\t' '!seen[$2,$3,$4,$5,$6,$7,$9,$10,$11,$12,$14,$15,$16]++' ../outputs/temp2.txt > ../outputs/temp3.txt
# cat $temp_file_output | sort -t$'\t' | uniq > $variant_output
#cat $temp_file_output | (sed -u 1q; sort) > $variant_output

# Remove worklist column
cut --complement -d$'\t' -f1 ../outputs/temp3.txt > ../outputs/temp4.txt

# Add primary key/ID column
awk '{printf "%s\t%s\n", NR==1 ? "variant_ID" : NR-1, $0}' ../outputs/temp4.txt > $variant_output

#awk '{printf "\"%s\"|%s\n", NR==1 ? "variant_ID" : NR-1, $0}' $variant_model

# Remove temporary files
rm $temp_file_output
rm ../outputs/temp2.txt
rm ../outputs/temp3.txt
rm ../outputs/temp4.txt

#awk '{print $1,$2,$3,$4,$9,$10,$11,$18,$24}' $input_file > $occurence_output
