"""Before adding worklist data, ensure worklist is complete.

Author: Katie Williams
Created: 12/01/2023
"""

import os
import csv

input_file = "../outputs/worklists_to_upload.txt"
new_updated_worklist_file = "../outputs/worklists_to_upload_PASS.txt"
rejected_worklist_file = "../outputs/worklists_to_upload_REJECTED.txt"


def worklist_sample_number(worklist):
    """Get the number of samples/patients in the worklist"""

    # open worklist / count number of lines
    worklist_file = "/data/worklistExome/" + worklist + "_SL.txt"
    with open(worklist_file, 'r') as fp:
        worklist_count = len(fp.readlines())

    return worklist_count - 2


def mutation_reports_number(worklist):
    """Get the number of mutation reports in the worklist"""

    folder = '/data/twist/' + worklist + '/starlims_upload/'
    files = [filename for filename in os.listdir(folder)
             if filename.endswith("_MutationReport.txt")]

    return len(files)


rejected_worklists = []

# Get worklists to upload, and store each worklist in an list
with open(input_file) as w:
    # This way ensures any empty lines are ignored
    worklists = [line.strip() for line in w if line.strip()]

    for worklist in worklists:

        print(worklist)

        # Count number of patients in worklist
        no_of_samples = worklist_sample_number(worklist)
        print(no_of_samples)

        # Navigate to /data/twist/
        no_of_reports = mutation_reports_number(worklist)
        print(no_of_reports)

        if no_of_samples != no_of_reports:
            print("Pipeline not complete")
            rejected_worklists.append(worklist)

    # check coverage also complete?

print(rejected_worklists)
new_list = [x for x in worklists if x not in rejected_worklists]

with open(new_updated_worklist_file, 'w') as o:

    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    for w in new_list:
        write.writerow([w])

with open(rejected_worklist_file, 'w') as o:

    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    for w in rejected_worklists:
        write.writerow([w])

# print(worklists)
