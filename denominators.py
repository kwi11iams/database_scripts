"""Calculate the denominators for each gene in each panel

Author: Katie Williams
Created: 27/05/2022
"""

import os
from pandas import read_csv
import csv


def getCoverageReportfilename(MID):
    """If in worklist folder, get coverage report filename for given MID."""
    prefixed = [filename for filename in os.listdir('.') if
                filename.endswith(str(MID) + "_CoverageReport.txt")]
    return prefixed


input_path = "../outputs/occurence_model.txt"
data_directory = "../../../twist"
output_wrt_twist = "../katie_dev/Cardiac_Dashboard/outputs/denominators.txt"

# Load file
data = read_csv(input_path, sep='\t', usecols=[0, 1, 2, 3])
# print(data.head())
# print(len(data))

unique = data.drop_duplicates(ignore_index=True)
# unique.reset_index(drop=True, inplace=True)
# print(unique.head())
# print(len(unique))

count = 0

# Navigate to data/twist
os.chdir(data_directory)
print(os.getcwd())

# Create empty output dictionary
denom = {}
# Fill with panels
panels = unique.drop_duplicates(subset="Panel", ignore_index=True)
# print(panels.Panel)
for panel in panels.Panel:
    denom[panel] = {}

# print(denom)

for i in range(0, len(unique)):

    # print(unique.MID[i])
    this_panel = unique.Panel[i]

    # Open twist worklist, and starlims_upload folder
    os.chdir(str(unique.Worklist[i]) + "/starlims_upload")
    # print(os.getcwd())

    # Add a check here to see if the pipeline has actually been run yet
    # Instance of empty starlims upload folder, so next part failed
    # Or caveat this in the getMutationReportfilename function...

    # Obtain file name
    report = getCoverageReportfilename(unique.MID[i])
    if len(report) == 0 and str(unique.Worklist[i]) == "1918202" and str(unique.MID[i] == "D705502"):
        print("It's a match!")
        os.chdir("../starlims_upload_new_coverage")
        report = getCoverageReportfilename(unique.MID[i])
    elif str(unique.Worklist[i]) == "1919065":
        print("Coverage reports wrong here...")
        os.chdir("../starlims_upload_new_coverage")
        report = getCoverageReportfilename(unique.MID[i])

    if len(report) == 1:
        report = report[0]

        # Open mutation report file and output file
        with open(report, 'r') as covreport:

            # read content from file
            coverage_lines = covreport.readlines()
            gene_count = 0
            for line in coverage_lines:
                # print(line.split('\t'))
                if line.split('\t')[0:4] == ['', '', '', '']:
                    gene = line.split('\t')[4]
                    gene_count += 1
                    # print(gene)

                    if gene in denom[this_panel].keys():
                        denom[this_panel][gene] += 1
                    # if gene not in dict add it
                    if gene not in denom[this_panel].keys():
                        denom[this_panel][gene] = 1
                        # print("Add " + gene + " to " + this_panel)

            # print(unique.Worklist[i], unique.MID[i], this_panel, gene_count)

    else:
        print("There are {} coverage report files for worklist {}, MID {}".format(
            len(report), unique.Worklist[i], unique.MID[i]))
        print(this_panel)
        print(count)
        # print("Script stopped.")
        # break

    # Exit data/twist/<worklist>/starlims_upload folder, to data/twist
    os.chdir("../..")

    count += 1
    print(count)
    # if count == 20:
    #     break

print(denom)

# Clear any previous output and create a blank file, with column headers
with open(output_wrt_twist, 'w') as o:
    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    write.writerow(["Panel", "Gene", "Total"])

# Output dictionary of dictionaries to file that can be uploaded to django
with open(output_wrt_twist, 'a') as denominators:
    # Append results to output files
    for panel in list(denom.keys()):
        for gene in list(denom[panel].keys()):
            row = [panel, gene, denom[panel][gene]]
            write = csv.writer(denominators, dialect='excel-tab')
            write.writerow(row)
