#!/bin/bash
 
cd $HOME
source CardiacDB_Env/bin/activate

cd /data/katie_dev/Cardiac_Dashboard/database_scripts

printf "### check_worklists.sh \n\n"
bash check_worklists.sh

echo "### compare_worklists.py"
python compare_worklists.py

echo "### check_worklists_complete.py"
python check_worklist_complete.py

echo "### cardiac_worklists.py"
python cardiac_worklists.py

echo "### collect_variants.py"
python collect_variants.py

echo "### refine_variant_list.py"
python refine_variant_list.py

printf "### create_variant_model.sh"
bash create_variant_model.sh

printf "### create_occurence_model.py"
python create_occurence_model.py

printf "### denominators.py"
python denominators.py

cd /data/katie_dev/Cardiac_Dashboard/outputs

# printf '### Copy files to webserver'
# scp variant_model.txt occurence_model.txt denominators.txt <user>@<web_server>:/data/Cardiac_Dashboard_Uploads

# printf '### Zip output files and save in /data/scripts/cardiac_dashboard'
# cd /data/katie_dev/Cardiac_Dashboard/
# zip -r /data/logs/cardiac_dashboard/uploads/CardiacDB_Files_$(date +\%Y\%m\%d\%H\%M\%S).zip outputs

