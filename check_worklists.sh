#!/bin/bash

cd ../../../worklistExome
ls | grep .txt | grep -x '^[0-9]\{7\}_SL.txt' | cut -f 1 -d '_' > ../katie_dev/Cardiac_Dashboard/outputs/worklistExome_filenames.txt
ls | grep .txt | cut -f 1 -d '.' | grep -xv '^[0-9]\{7\}' | grep -xv '^[0-9]\{7\}_SL' | grep -vE 'epi|cranio|mito|Mito|melanoma|test|reanalysis|WTCHG|HSQ|MSQ|^P|worklist_exome|long' > ../katie_dev/Cardiac_Dashboard/outputs/alternative_filenames.txt
