"""Script 5, Create the occurence model text file for djnago

Author: Katie Williams

Input: refined_list.txt
Output: occurence_model.txt
"""

import csv

# File locations
input_file = "../outputs/refined_list.txt"
occurence_output = "../outputs/occurence_model.txt"
variant_model = "../outputs/variant_model.txt"

with open(occurence_output, 'w') as o:

    # Using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')

# Open input and output files
with open(input_file, 'r') as input, open(variant_model, 'r') as v_model, open(occurence_output, 'a') as output:

    # Read file content
    refined_lines = csv.reader(input, delimiter='\t')
    variants = csv.reader(v_model, delimiter='\t')

    # Output header to occurence model
    header = next(refined_lines)
    # occurence_header = header[0:4] + header[8:11] + [header[17], header[23], "variant_ID"]
    occurence_header = header[0:4] + header[8:11] + \
        [header[13], header[17], header[23], "variant_ID"]
    write = csv.writer(output, dialect='excel-tab')
    write.writerow(occurence_header)

    i = 0  # Counter

    for line in refined_lines:

        i = i+1  # Counter

        # Print counter every 1000 lines
        if i % 1000 == 0:
            print(i)

        # Select relevant variant columns from refined_list file
        # Don't include VariantType or Alamut
        var_model = line[4:8] + line[11:13] + line[14:17] + [line[18]] + line[20:23]
        g_nomen = line[14]

        # Reset the variants loop back to the beginning
        v_model.seek(0)
        next(variants, None)  # skip header

        # Loop through indexed variant model file to find match
        for v in variants:

            v_g_nomen = v[8]

            # First match by genomic nomenclature
            if g_nomen == v_g_nomen:

                if var_model == v[1:7] + v[8:12] + v[13:]:

                    # Output occurence with variant_ID
                    occurence = line[0:4] + line[8:11] + [line[13], line[17], line[23]]
                    occurence.append(v[0])  # variant_ID
                    write = csv.writer(output, dialect='excel-tab')
                    write.writerow(occurence)
                    break

                else:
                    continue

            else:
                continue
