
"""Script 1, select the relevant cardiac panels from the worklist files.

Author: Katie Williams
Date created: 24/02/22

Input: audit_twist_worklists.txt
Output: cardiac_worklists.txt
"""

import os
import csv
import numpy as np

# ***CHANGE THIS TO YOUR DEV FOLDER AT /data/ ON THE SERVER***
dev_folder = "USER_dev"

# Cardiac panels to include
cardiac_panels = ["R131", "R132", "R133"]
cardiac_panels_names = np.array(["R131_Twist_HCM",
                                 "R132_Twist_DCM-ArCM",
                                 "R133_Twist_ARVC"])

# Initialise list for output of results
cardiac_worklists = []

# State working directories
worklists_location = "../outputs/worklists_to_upload_PASS.txt"
data_directory = "../../../worklistExome"
output_directory = f"../{dev_folder}/Cardiac_Dashboard/outputs"

# Read the list of worklists
with open(worklists_location) as w:
    # This way ensures any empty lines are ignored
    folders = [line.strip() for line in w if line.strip()]

print("There are " + str(len(folders)) + " worklists.")

# Move to directory containing the worklist folders
os.chdir(data_directory)

# Loop through each worklist
for worklist in folders:

    # Do the text files exist?
    # NB: Only looking at _SL.txt files (not reanalysis files)
    if worklist in ["1917866", "1917869"]:
        # Alt names for early worklists (exceptions)
        worklist_file = worklist + "_SL_STARLIMS.txt"
    else:
        worklist_file = worklist + "_SL.txt"

    if os.path.exists(os.path.join(os.getcwd(), worklist_file)):

        # Open the text file
        with open(worklist_file) as f:

            contents = csv.reader(f, delimiter='\t')

            for line in contents:

                if any(panel in line[5] for panel in cardiac_panels):
                    # GET Worklist, StarLIMS_ID, MID
                    StarLIMS_ID = line[2]
                    MID = line[4]
                    # panel = line[5]
                    panel = cardiac_panels_names[np.array(
                        list(panel in line[5] for panel in cardiac_panels))][0]
                    cardiac_worklists.append([worklist, StarLIMS_ID, MID,
                                              panel])

    else:
        print("Reanalysis or no txt file - ignored worklist " + worklist)

# Export to excel spreadsheet
os.chdir(output_directory)  # Set wd

# Create header (column names in output - cardiac_worklists)
columns = ['Worklist', 'StarLIMS_ID', 'MID', 'Panel']

# Create tab delimited file of results
with open('cardiac_worklists.txt', 'w') as o:

    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    write.writerow(columns)
    write.writerows(cardiac_worklists)
