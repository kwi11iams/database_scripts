# Cardiac Dashboard - Database Scripts #
Katie Williams <br>
README Last Updated: 14/04/2023
----------------------
This repository (database_scripts) contains the python scripts to generate a list of all variants found within 
cardiac testing panels:

* R131_Twist_HCM, 
* R132_Twist_DCM-ArCM, and 
* R133_Twist_ARVC. 

The final output files will be used as the database for the djnago cardiac dashboard web app. <br>
The BitBucket repository for this django application can be found 
[here](#https://bitbucket.org/kwi11iams/django_dashboard). <br>

## 1. Installation ##
These files must be copied to a folder on the main server at `/data/<user_dev>/Cardiac_Dashboard`. <br>
Also create another folder `outputs` at this same location, `/data/<user_dev>/Cardiac_Dashboard`. 

You will need to add your `user_dev` folder to some of the scripts in the pipeline. 

## 2. Requirements ##

* pandas 1.1.5

## 3. Pipeline

![Workflow](https://bitbucket.org/kwi11iams/database_scripts/downloads/CardiacDatabasePipeline_resized.png)

The bash script `cardiac_dashboard_pipeline` can runs the required scripts in this order:

1. [check_worklists.sh](#script6)
2. [compare_worklists.py](#script8)
3. [check_worklists_complete.py](#script7)
4. [ cardiac_worklists.py ](#script1)
5. [ collect_variants.py ](#script2)
6. [ refine_variant_list.py ](#script3)
7. [create_variant_model.sh](#script4)
8. [create_occurence_model.py](#script5)
9. [denominators.py](#script9)


## 4. Files explained ##

#### audit_twist_worklists.txt 
This text file represents the list of worklists to analyse in this pipeline, with each worklist number on a new line. <br><br>


<a name="script6"></a>
#### check_worklists.sh

Output: `worklistExome_filenames.txt`, `alternative_filenames.txt`
<br>

Generates two output files, bases on the worklist files on the main server at /data/worklistExome. <br>
`worklistExome_filenames.txt`lists all the files that end in _SL.txt
`alternative_filenames.txt` lists all the files that have unconventional file names 
(i.e. not a 7-digit number or a 7-digit number followed by _SL.txt), 
excluding files that include 'epi' or 'cranio' in the filename. 


<a name="script8"></a>
#### compare_worklists.py

Input: `worklistExome_filenames.txt`, `audit_twist_worklists.txt`<br>
Output: `worklists_to_upload.txt`

Compares the two input files to identify any mismatches in the worklists. i.e. worklists found in the audit that are not in /data/worklistExome and vice versa. Also checks for any duplicates in each input txt file. 


<a name="script7"></a>
#### check_worklists_complete.py

Input: `worklists_to_upload.txt`<br>
Outputs: `worklists_to_upload_PASS.txt`, `worklists_to_upload_REJECTED.txt`

Ensure the worklist is complete before trying to collate the data. Script included for a check when automating the uploads. 


<a name="script1"></a>
#### cardiac_worklists.py

Input: `audit_twist_worklists.txt`<br>
Output: `cardiac_worklists.txt`

Uses the list of audit worklists, to search `data/worklistExome` files on the main server. <br>
Extracts the worklist, StarLIMS ID, MID and panel which test for cardiac panels.

*NB that this doesn't currently include renanalyses*<br><br>


<a name="script2"></a>
#### collect_variants.py

Input: `cardiac_worklists.txt`<br>
Output: `variant_list.txt`

Creates a list of all the variants from mutation reports associated with cardiac testing panels. <br>
This only selects the relevant fields for each variant. <br><br>

<a name="script3"></a>
#### refine_variant_list.py

Input: `variant_list.txt`<br>
Output: `refined_list.txt`, `artefact_list.txt`

Analyses the initial list of variants and

* removes artefacts, and outputs them to a seperate file (`artefact_list.txt`)
* Flags possible artefacts
* Removes any renanalysis occurence of variants, such that variants are only included from the first panel for that individual

<a name="script4"></a>
#### create_variant_model.sh

Input: `refined_list.txt`<br>
Output: `variant_model.txt`

Creates a file compatible for upload to the Django Cardiac Dashboard database, for the Variant model. 

<a name="script5"></a>
#### create_occurence_model.sh

Input: `refined_list.txt`, `variant_model.txt`<br>
Output: `occurence_model.txt`

Creates a file compatible for upload to the Django Cardiac Dashboard database, for the Occurence model. 


<a name="script9"></a>
#### denominators.py

Input: `occurence_model.txt`<br>
Output: `denominators.txt`

Obtains the total number of samples looked at for each panel. Needed for upload to the database.
