"""Script 2, collate the variants from cardiac panels.

Author: Katie Williams
Date created: 06/04/22

Input: cardiac_worklists.txt
Output: variant_list.txt
"""

import pandas as pd
import os
import csv

# ***CHANGE THIS TO YOUR DEV FOLDER AT /data/ ON THE SERVER***
dev_folder = "USER_dev"


def getMutationReportfilename(MID):
    """If in worklist folder, get mutation report filename for given MID."""
    prefixed = [filename for filename in os.listdir('.') if
                filename.endswith(str(MID) + "_MutationReport.txt")]
    return prefixed


# File locations
input = "../outputs/cardiac_worklists.txt"
data_directory = "../../../twist"
output = "../outputs/variant_list.txt"
output_wrt_starlimsupload = (f"../../../{dev_folder}/Cardiac_Dashboard/outputs/variant_list.txt")

# Columns for output
worklistcolumns = ["Worklist", "StarLIMS_ID", "MID", "Panel"]
variantcolumns = [
    "ReferencePosition", "Gene", "Reference",
    "AlternativeAllele", "Caller(Cov;CovF;CovR)",
    "Caller(%Variant)", "Caller(Score)", "RefSeq", "Chr",
    "VariantType", "GenomeChange", "MutationCall",
    "ProteinChange", "Variant", "GenomicCoordinate", "Alamut",
    "HGMD_DNA", "Build"
]

# Clear any previous output and create a blank file, with column headers
with open(output, 'w') as o:

    # using csv.writer method from CSV package
    write = csv.writer(o, dialect='excel-tab')
    write.writerow(worklistcolumns + variantcolumns)

# Load input file
df = pd.read_csv(input, sep="\t")

# Navigate to data/twist
os.chdir(data_directory)

# Open relevant mutation report files
# Loop through each line of input (cardiac_worklists.txt)
for i in range(0, len(df)):

    # Open twist worklist, and starlims_upload folder
    os.chdir(str(df.Worklist[i]) + "/starlims_upload")

    # Add a check here to see if the pipeline has actually been run yet
    # Instance of empty starlims upload folder, so next part failed
    # Or caveat this in the getMutationReportfilename function...

    # Obtain file name
    report = getMutationReportfilename(df.MID[i])
    if len(report) == 1:
        report = report[0]
    else:
        print(f"There are {len(report)} mutation report files for worklist {df.Worklist[i]}, MID {df.MID[i]}")
        print("Script stopped.")
        break

    # Open mutation report file and output file
    with open(report, 'r') as mutreport:
        with open(output_wrt_starlimsupload, 'a') as variantlist:

            # read content from first file
            # Ignore first 4 lines, variants start on line 5
            variant_lines = mutreport.readlines()[4:]

            for line in variant_lines:

                # Add StarLIMS ID, MID, worklist, panel
                worklist = df.Worklist[i]
                starlims_id = df.StarLIMS_ID[i]
                mid = df.MID[i]
                panel = df.Panel[i]

                c1 = [worklist, starlims_id, mid, panel]

                # Select relevant columns
                c1 = c1 + line.split("\t")[1:9]
                c1.append(line.split("\t")[10])
                c1.append(line.split("\t")[13])
                c1.append(line.split("\t")[15])

                # c nomenclature
                c_nomen = line.split("\t")[16]
                if c_nomen != "NA":
                    c_nomen_edited = "c." + c_nomen.split("[")[1].split("]")[0]
                    c1.append(c_nomen_edited)
                else:
                    c1.append(c_nomen)

                # p_nomenclature
                p_nomen = line.split("\t")[17]
                if p_nomen != "NA":
                    p_nomen_edited = "p." + p_nomen.split("[")[1].split("]")[0]
                    c1.append(p_nomen_edited)
                else:
                    c1.append(p_nomen)

                c1 = c1 + line.split("\t")[19:21]

                # Curate alamut column if empty
                # 21 is alamut
                # 15 is genome_change
                genome_change = line.split("\t")[15]
                alamut = line.split("\t")[21]
                if alamut == "":
                    alamut = genome_change.replace('g.', '')
                    # print("Alamut")
                c1.append(alamut)

                # if blank, put NA? This column has a lot of blanks
                if line.split("\t")[23] == "":
                    # blank
                    HGMD_DNA = "NA"
                else:
                    HGMD_DNA = line.split("\t")[23]
                c1.append(HGMD_DNA)

                # Obtain build from GenomicCoordinate column
                gc = line.split("\t")[20]
                build = gc.split(":")[0].split(".")[1]
                c1.append(build)

                # Append variants to output file
                write = csv.writer(variantlist, dialect='excel-tab')
                write.writerow(c1)

    # Exit data/twist/<worklist>/starlims_upload folder, to data/twist
    os.chdir("../..")
